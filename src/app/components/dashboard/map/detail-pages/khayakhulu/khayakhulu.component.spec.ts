import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KhayakhuluComponent } from './khayakhulu.component';

describe('KhayakhuluComponent', () => {
  let component: KhayakhuluComponent;
  let fixture: ComponentFixture<KhayakhuluComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KhayakhuluComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KhayakhuluComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
