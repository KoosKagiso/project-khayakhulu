import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelverdientComponent } from './welverdient.component';

describe('WelverdientComponent', () => {
  let component: WelverdientComponent;
  let fixture: ComponentFixture<WelverdientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelverdientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelverdientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
