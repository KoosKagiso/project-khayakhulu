import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmahlubiDialogComponent } from './amahlubi-dialog.component';

describe('AmahlubiDialogComponent', () => {
  let component: AmahlubiDialogComponent;
  let fixture: ComponentFixture<AmahlubiDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmahlubiDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmahlubiDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
