import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-amahlubi-dialog',
  templateUrl: './amahlubi-dialog.component.html',
  styleUrls: ['./amahlubi-dialog.component.scss']
})
export class AmahlubiDialogComponent implements OnInit {
  villages = [
    {
      villageName: 'Khayakhulu',
      pageLink: '',
    },
    {
      villageName: 'Welverdient',
      pageLink: '',
    },
    {
      villageName: 'KwaNolanga',
      pageLink: '',
    },
    {
      villageName: 'Welgevaal',
      pageLink: '',
    },
    {
      villageName: 'Goedehoop',
      pageLink: '',
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
