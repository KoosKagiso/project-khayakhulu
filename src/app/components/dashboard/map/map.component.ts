import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';

'@angular/core';
import { AgmCoreModule } from '@agm/core';
import { MapDialogComponent } from './map-dialog/map-dialog.component';
import { AmahlubiDialogComponent } from './amahlubi-dialog/amahlubi-dialog.component';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements AfterViewInit {
  constructor(public dialog: MatDialog) {}

  openDialog() {
    this.dialog.open(MapDialogComponent);
  }

  villages = [
    {
      villageName: 'Khayakhulu',
      pageLink: '',
    },
    {
      villageName: 'Welverdient',
      pageLink: '',
    },
    {
      villageName: 'KwaNolanga',
      pageLink: '',
    },
    {
      villageName: 'Welgevaal',
      pageLink: '',
    },
    {
      villageName: 'Goedehoop',
      pageLink: '',
    },
  ];

  @ViewChild('mapContainer', { static: false }) gmap: ElementRef;

  map: google.maps.Map;

  amahlubiLongitude = 26.660131;
  amahlubiLatitude = -25.101181;

  mgwalanaLongitude = 25.5878649;
  mgwalanaLatitude = -33.8181119;

  royalBafokengLongitude = 27.160813;
  royalBafokengLatitude = -25.5787724;

  amahlubiCoordinates = new google.maps.LatLng(
    this.amahlubiLatitude,
    this.amahlubiLongitude
  );

  mgwalanaCoordinates = new google.maps.LatLng(
    this.mgwalanaLatitude,
    this.mgwalanaLongitude
  );

  royalBafokengCoordinates = new google.maps.LatLng(
    this.royalBafokengLatitude,
    this.royalBafokengLongitude
  );

  amahlubiMapOptions: google.maps.MapOptions = {
    center: this.amahlubiCoordinates,
    zoom: 5,
  };

  mgwalanaMapOptions: google.maps.MapOptions = {
    center: this.mgwalanaCoordinates,
    zoom: 5,
  };

  royalBafokengMapOptions: google.maps.MapOptions = {
    center: this.royalBafokengCoordinates,
    zoom: 5,
  };

  amahlubiMarker = new google.maps.Marker({
    position: this.amahlubiCoordinates,
    map: this.map,
    title: 'Amahlubi',
    icon: '../../../assets/blue-icon.png',
  });

  mgwalanaMarker = new google.maps.Marker({
    position: this.mgwalanaCoordinates,
    map: this.map,
    title: 'Mgwalana',
    icon: '../../../assets/green-pin.png',
  });

  royalBafokengMarker = new google.maps.Marker({
    position: this.royalBafokengCoordinates,
    map: this.map,
    title: 'Royal Bafokeng',
    icon: '../../../assets/brown-pin.png',
  });

  ngAfterViewInit() {
    this.mapInitializer();
  }

  mapInitializer(): void {
    this.map = new google.maps.Map(
      this.gmap.nativeElement,
      this.amahlubiMapOptions
    );
    this.map = new google.maps.Map(
      this.gmap.nativeElement,
      this.mgwalanaMapOptions
    );
    this.map = new google.maps.Map(
      this.gmap.nativeElement,
      this.royalBafokengMapOptions
    );

    this.mgwalanaMarker.setMap(this.map);
    this.amahlubiMarker.setMap(this.map);
    this.royalBafokengMarker.setMap(this.map);

    this.amahlubiMarker.addListener('click', () => {
      this.dialog.open(MapDialogComponent);
    });

    this.mgwalanaMarker.addListener('click', () => {
      this.dialog.open(AmahlubiDialogComponent);
    });
  }
}
