import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA,MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-map-dialog',
  templateUrl: './map-dialog.component.html',
  styleUrls: ['./map-dialog.component.scss']
})
export class MapDialogComponent implements OnInit {
  villages = [
    {
      villageName: 'Khayakhulu',
      pageLink: '',
    },
    {
      villageName: 'Welverdient',
      pageLink: '',
    },
    {
      villageName: 'KwaNolanga',
      pageLink: '',
    },
    {
      villageName: 'Welgevaal',
      pageLink: '',
    },
    {
      villageName: 'Goedehoop',
      pageLink: '',
    },
  ];
  constructor() { }

  ngOnInit(): void {
  }

}
