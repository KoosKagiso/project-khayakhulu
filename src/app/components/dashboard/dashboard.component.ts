import { Component, OnInit } from '@angular/core';
// import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Subscription } from 'rxjs';
import { UsersService } from '../../services/users.service';
import { Routes, RouterModule, Router } from '@angular/router';
declare const $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  opened = true;
  over = 'side';
  expandHeight = '42px';
  collapseHeight = '42px';
  displayMode = 'flat';
  isActive: boolean = true;
  userDetails ;

  constructor(public userService: UsersService, private router: Router) { }


  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(
      res=>{
        this.userDetails = res['user'];
      },
      err=>{}
    )
  }

  logout() {
    this.userService.deleteToken();
    this.router.navigate(['./login']);
  }

//   isMobileMenu() {
//     if ($(window).width() > 991) {
//         return false;
//     }
//     return true;
// };

// overlap = false;


  // watcher: Subscription;

  // constructor(media: ObservableMedia) {
  //   this.watcher = media.subscribe((change: MediaChange) => {
  //     if (change.mqAlias === 'sm' || change.mqAlias === 'xs') {
  //       this.opened = false;
  //       this.over = 'over';
  //     } else {
  //       this.opened = true;
  //       this.over = 'side';
  //     }
  //   });
  // }

}
