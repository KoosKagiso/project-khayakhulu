import { Component, OnInit , ViewEncapsulation} from '@angular/core';
import { ImageService } from '../../../services/image.service';

@Component({
  selector: 'app-social-content',
  templateUrl: './social-content.component.html',
  styleUrls: ['./social-content.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class SocialContentComponent implements OnInit {

  images = [];
  slideIndex = 0;


  cards = [
   {
    title: "What is COVID-19?",
    details: "On 31 December 2019, the World Health Organization (WHO) reported a cluster of pneumonia cases in Wuhan City, China. ‘Severe Acute Respiratory Syndrome Coronavirus 2’ (SARS-CoV-2) was confirmed as the causative agent of what we now know as ‘Coronavirus Disease 2019’ (COVID-19). Since then, the virus has spread to more than 100 countries, including South Africa."
   },
   {
    title: "How can you prevent infection?",
    details: "Wash your hands often with soap and water for at least 20 seconds. If soap and water are not available, use an alcohol-based hand sanitiser.Avoid touching your eyes, nose, and mouth with unwashed hands. Avoid close contact with people who are sick.Stay at home when you are sick and try and keep a distance from others at home."
   },
   {
    title: "Links",
        link : "https://www.covidvisualizer.com"
   },
   
]

cards1 = [
  {
    titleGBV: "Home is not a safe place for me. What can I do ?",
    detailsGBV: "If you are experiencing violence, it may be helpful to reach out to family, friends and neighbors, to seek support from a hotline or, if safe, from online service for survivors of violence. Find out if local services (e.g. shelters, counselling) are open and reach out to them if available. Make a safety plan in case the violence against you or your children escalates."
   },
   {
    titleGBV: "I am worried about someone I know.How can I help ??",
    detailsGBV: "Keep regularly in touch with the person to check that they are safe, ensuring that it is safe for them to be in touch with you.If someone you know needs urgent help for whatever reason, be prepared to call emergency health services, the police, health centre, or hotline. "
     
   },
   {
    titleGBV: "Useful numbers",
   imgGBV : "../../../assets/GBV.png"
   },
  
]




  constructor(private imageService: ImageService) {}

  ngOnInit(): void {
    this.loadImages();
  }

  loadImages(): void {
    this.imageService.fetchImages()
     .subscribe(images => this.images = images);
   }
   openModal() {
    document.getElementById('imgModal').style.display = "block";
   }
   closeModal() {
    document.getElementById('imgModal').style.display = "none";
   }
   plusSlides(n) {
    this.showSlides(this.slideIndex += n);
   }
   currentSlide(n) {
    this.showSlides(this.slideIndex = n);
   }
   showSlides(slideIndex);
   showSlides(n) {
    let i;
    const slides = document.getElementsByClassName("img-slides") as HTMLCollectionOf < HTMLElement > ;
    const dots = document.getElementsByClassName("images") as HTMLCollectionOf < HTMLElement > ;
    if (n > slides.length) {
     this.slideIndex = 1
    }
    if (n < 1) {
     this.slideIndex = slides.length
    }
    for (i = 0; i < slides.length; i++) {
     slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
     dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[this.slideIndex - 1].style.display = "block";
    if (dots && dots.length > 0) {
     dots[this.slideIndex - 1].className += " active";
    }
   }
  }


