import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentsPayoutsComponent } from './payments-payouts.component';

describe('PaymentsPayoutsComponent', () => {
  let component: PaymentsPayoutsComponent;
  let fixture: ComponentFixture<PaymentsPayoutsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaymentsPayoutsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentsPayoutsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
