import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  events = [
    {
      name : "First Annual Khayakhulu Event",
      description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta magni ab aperiam, repudiandae quam dicta alias illo repellendus quisquam labore laudantium veritatis deserunt dolorum harum ea dolore enim! Molestiae, nemo",
      date : "06",
      month : "oct"
    },
    {
      name : "Second Annual Khayakhulu Event",
      description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta magni ab aperiam, repudiandae quam dicta alias illo repellendus quisquam labore laudantium veritatis deserunt dolorum harum ea dolore enim! Molestiae, nemo",
      date : "01",
      month : "July"
    },
    {
      name : "Third Annual Khayakhulu Event",
      description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta magni ab aperiam, repudiandae quam dicta alias illo repellendus quisquam labore laudantium veritatis deserunt dolorum harum ea dolore enim! Molestiae, nemo",
      date : "25",
      month : "November"
    },
    {
      name : "Fourth Annual Khayakhulu Event",
      description : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Soluta magni ab aperiam, repudiandae quam dicta alias illo repellendus quisquam labore laudantium veritatis deserunt dolorum harum ea dolore enim! Molestiae, nemo",
      date : "16",
      month : "December"
    },

  ]
  constructor() { }

  ngOnInit(): void {
  }

}
