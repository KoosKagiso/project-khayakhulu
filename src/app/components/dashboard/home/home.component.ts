import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../services/users.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  cards = [
    {
      icon: "person",
      cardName: "Cultural Content"
    },
    {
      icon: "language",
      cardName: "Social & Economic Content"
    },
    {
      icon: "image",
      cardName: "Image Library"
    }
  ]

  userDetails ;
  constructor(public userService: UsersService) { }

  ngOnInit(): void {
    this.userService.getUserProfile().subscribe(
      res=>{
        this.userDetails = res['user'];
      },
      err=>{}
    )
  }

}
