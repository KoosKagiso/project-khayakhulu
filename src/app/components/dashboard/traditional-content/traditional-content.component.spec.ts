import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraditionalContentComponent } from './traditional-content.component';

describe('TraditionalContentComponent', () => {
  let component: TraditionalContentComponent;
  let fixture: ComponentFixture<TraditionalContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraditionalContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraditionalContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
