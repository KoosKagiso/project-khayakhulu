import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-traditional-content',
  templateUrl: './traditional-content.component.html',
  // template: `
  // <pdf-viewer [src]="pdfSrc" 
  //             [render-text]="true"
  //             style="display: block;"
  // ></pdf-viewer>
  // `,
  styleUrls: ['./traditional-content.component.scss']
})
export class TraditionalContentComponent implements OnInit {
  pdfSrc = "/assets/Zibi.pdf";
  constructor() { }

  ngOnInit(): void {
  }

}
