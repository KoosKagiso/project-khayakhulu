export class User {
  fullName: string;
  membershipNumber:string;
  email: string;
  clanName:string;
  village:string;
  password: string;
}
