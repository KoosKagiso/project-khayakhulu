import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './components/user/registration/registration.component';
import { LoginComponent } from './components/user/login/login.component';
import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { AuthenticationGuard } from './auth/authentication.guard';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HomeComponent } from './components/dashboard/home/home.component';
import { PaymentsPayoutsComponent } from './components/dashboard/payments-payouts/payments-payouts.component';
import { NotificationsComponent } from './components/dashboard/notifications/notifications.component';
import { TraditionalContentComponent } from './components/dashboard/traditional-content/traditional-content.component';
import { SocialContentComponent } from './components/dashboard/social-content/social-content.component';
import { MapComponent } from './components/dashboard/map/map.component';
import { CalendarComponent } from './components/dashboard/calendar/calendar.component'

import { from } from 'rxjs';
import { KhayakhuluComponent } from './components/dashboard/map/detail-pages/khayakhulu/khayakhulu.component';


const routes: Routes = [
  {
    path: "register", component : RegistrationComponent
  },
  {
    path: "login", component : LoginComponent
  },
  {
    path: 'dashboard', canActivate: [AuthenticationGuard], component: DashboardComponent,
    children: [
      {path: '', canActivate: [AuthenticationGuard],component: HomeComponent},
      { path: 'home', canActivate: [AuthenticationGuard],  component: HomeComponent },
      { path: 'profile', canActivate: [AuthenticationGuard],  component: UserDetailsComponent },
      {path: 'payments-payouts', component: PaymentsPayoutsComponent},
      {path: 'notifications', component: NotificationsComponent},
      {path: 'traditional-content', component: TraditionalContentComponent},
      {path: 'social-economic', component: SocialContentComponent},
      {path: 'map', component: MapComponent},
      {path: 'calendar', component: CalendarComponent},
    ]
  },
  {
    path: 'khayakhulu', component : KhayakhuluComponent
  },
  {
    path: '', redirectTo: '', pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
