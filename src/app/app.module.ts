import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule} from '@angular/forms';
import { MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatIconModule} from '@angular/material/icon';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { UserDetailsComponent } from './components/user/user-details/user-details.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegistrationComponent } from './components/user/registration/registration.component';
import { ContentComponent } from './components/content/content.component';
import {MatMenuModule} from '@angular/material/menu';
import {UsersService} from './services/users.service';
import { from } from 'rxjs';
import { AuthenticationGuard } from './auth/authentication.guard';
import { AuthenticationInterceptor } from './auth/authentication.interceptor';
import { MainLayoutComponent } from './components/layout/main-layout/main-layout.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { HomeComponent } from './components/dashboard/home/home.component';
import { PaymentsPayoutsComponent } from './components/dashboard/payments-payouts/payments-payouts.component';
import { NotificationsComponent } from './components/dashboard/notifications/notifications.component';
import { TraditionalContentComponent } from './components/dashboard/traditional-content/traditional-content.component';
import { SocialContentComponent } from './components/dashboard/social-content/social-content.component';
import { MapComponent } from './components/dashboard/map/map.component';
import { AgmCoreModule } from '@agm/core';
import {MatDialogModule} from '@angular/material/dialog';
import * as $ from 'jquery';
//import { GoogleMapsModule } from '@angular/google-maps';
import { YouTubePlayerModule } from "@angular/youtube-player";
import { CalendarComponent } from './components/dashboard/calendar/calendar.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { ImageService } from './services/image.service';
import {MatTabsModule} from '@angular/material/tabs';
import { MapDialogComponent } from './components/dashboard/map/map-dialog/map-dialog.component';
import { AmahlubiDialogComponent } from './components/dashboard/map/amahlubi-dialog/amahlubi-dialog.component';
import { KhayakhuluComponent } from './components/dashboard/map/detail-pages/khayakhulu/khayakhulu.component';
import { WelverdientComponent } from './components/dashboard/map/detail-pages/welverdient/welverdient.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UserDetailsComponent,
    LoginComponent,
    RegistrationComponent,
    ContentComponent,
    MainLayoutComponent,
    DashboardComponent,
    HomeComponent,
    PaymentsPayoutsComponent,
    NotificationsComponent,
    TraditionalContentComponent,
    SocialContentComponent,
    MapComponent,
    CalendarComponent,
    MapDialogComponent,
    AmahlubiDialogComponent,
    KhayakhuluComponent,
    WelverdientComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    MatButtonModule,
    MatCardModule,
    MatToolbarModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatIconModule,
    HttpClientModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAFgM81Qz-SwfTzUsr4F51AgDj0HdN88CQ'
    }),
    MatDialogModule,
    YouTubePlayerModule,
    PdfViewerModule,
    NgxImageGalleryModule,
    MatTabsModule
    // GoogleMapsModule

  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthenticationInterceptor,
    multi: true
  },AuthenticationGuard, UsersService,ImageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
