import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as $ from "jquery";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})



export class AppComponent implements OnInit {
  showList: boolean = false;

   router: string;

  constructor(public _router: Router) {
    // this.router = _router.url;

  }

  ngOnInit() {
    this.checkRouter();
    $('body').addClass('df');
  }

  checkRouter() {

    this._router.events.subscribe(()=> {
      this.router = this._router.url;
      // if(this.router == '/login'){
      //   this.router = '/login';
      //   console.log("helloddd login", this.router);
      // }
    });

    return this.router;

    // console.log("current router its", this.router);
  }
}
