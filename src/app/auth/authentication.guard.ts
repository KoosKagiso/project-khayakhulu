import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { UsersService } from "../services/users.service";
import { Router } from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  constructor(private userService : UsersService,private router : Router){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if (!this.userService.isLoggedIn()) {
        this.router.navigateByUrl('/login');
        this.userService.deleteToken();
        return false;
      }
    return true;
  }

}

// this._router.events.subscribe( responseRouter => {
  //   this.router = this._router.url ;
  //   this.routeSubsri = responseRouter;
  //   console.log("router from sub",this.router );
  //   console.log("current subscribed from response is",this.routeSubsri.url );
  // });
