# KhayakhuluProject

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

## To run the Project

-- run Npm Install to add all the dependecies then run the below commands

## Front End Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Backend Development server 
 Run `npm run server` for a node server. Navigte to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

 ## Front End Development server & Backend Development server 
 Run `npm run start` to run backend an frontend concurrently. Navigte to  `http://localhost:4200/` and `http://localhost:3000/`. The app will automatically reload if you change any of the source files.


 To see users Navigate to `http://localhost:3000/api/users` to access api for the users.

 If you cant run `npm run server` please install  ( npm i --save-dev nodemon ) - We use this for node monitor any changes we doin on our server or backend

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).


## Branching Strategy

naming convesion example can be as follows

kagiso__bug-fix
thabo__service-authentication

and so on
